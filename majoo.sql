-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Mar 2020 pada 05.13
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `majoo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `KD_ADMIN` int(11) NOT NULL,
  `USERNAME` varchar(55) NOT NULL,
  `PASSWORD` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`KD_ADMIN`, `USERNAME`, `PASSWORD`) VALUES
(1, 'ridjal', '25f9e794323b453885f5181f1b624d0b');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail`
--

CREATE TABLE `tb_detail` (
  `KD_DETAIL` int(11) NOT NULL,
  `KD_PEMBELIAN` int(11) NOT NULL,
  `KD_PRODUK` int(11) NOT NULL,
  `JUMLAH` int(11) NOT NULL,
  `TOTAL` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_detail`
--

INSERT INTO `tb_detail` (`KD_DETAIL`, `KD_PEMBELIAN`, `KD_PRODUK`, `JUMLAH`, `TOTAL`) VALUES
(1, 0, 4, 1, 2750000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pembelian`
--

CREATE TABLE `tb_pembelian` (
  `KD_PEMBELIAN` varchar(11) NOT NULL,
  `KD_USER` int(11) DEFAULT NULL,
  `TGL_PEMBELIAN` text,
  `TOTAL` int(11) DEFAULT NULL,
  `STATUS` varchar(55) NOT NULL,
  `BUKTI` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_produk`
--

CREATE TABLE `tb_produk` (
  `KD_PRODUK` int(22) NOT NULL,
  `NAMA_PRODUK` varchar(55) NOT NULL,
  `DESKRIPSI` text NOT NULL,
  `BERAT` int(11) NOT NULL,
  `HARGA` int(11) NOT NULL,
  `STOCK` int(11) NOT NULL,
  `FOTO` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_produk`
--

INSERT INTO `tb_produk` (`KD_PRODUK`, `NAMA_PRODUK`, `DESKRIPSI`, `BERAT`, `HARGA`, `STOCK`, `FOTO`) VALUES
(6, 'Majoo Pro', 'Deksripsi produk Deksripsi produk Deksripsi produk', 2, 2750000, 5, 'standard_repo6.png'),
(7, 'Majoo Advance', 'Deksripsi produk Deksripsi produk Deksripsi produk Deksripsi produk', 2, 2750000, 0, 'paket-advance1.png'),
(8, 'Majoo Life Style', 'Deksripsi produk Deksripsi produk Deksripsi produk Deksripsi produk Deksripsi produk', 5, 2750000, 0, 'paket-lifestyle.png'),
(9, 'Majoo Desktop', 'Deksripsi produk Deksripsi produk Deksripsi produk Deksripsi produk', 3, 2750000, 0, 'paket-desktop.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `KD_USER` int(11) NOT NULL,
  `NAMA_USER` varchar(55) NOT NULL,
  `NO_TELP` text NOT NULL,
  `EMAIL` text NOT NULL,
  `PASSWORD` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`KD_USER`, `NAMA_USER`, `NO_TELP`, `EMAIL`, `PASSWORD`) VALUES
(2, 'ridjal', '1111', 'ridjal@gmail.com', '25f9e794323b453885f5181f1b624d0b');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`KD_ADMIN`);

--
-- Indeks untuk tabel `tb_detail`
--
ALTER TABLE `tb_detail`
  ADD PRIMARY KEY (`KD_DETAIL`);

--
-- Indeks untuk tabel `tb_pembelian`
--
ALTER TABLE `tb_pembelian`
  ADD PRIMARY KEY (`KD_PEMBELIAN`),
  ADD KEY `KD_USER` (`KD_USER`);

--
-- Indeks untuk tabel `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`KD_PRODUK`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`KD_USER`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `KD_ADMIN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_detail`
--
ALTER TABLE `tb_detail`
  MODIFY `KD_DETAIL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `KD_PRODUK` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `KD_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
