<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function cek_user()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$query = $this->db->where('username', $username)
			->where('password', md5($password))
			->get('tb_admin');
			
			if($query->num_rows() > 0){
				$data = array(
					'username' => $username,
					'userlevel' => 'admin',
					'logged_in' => TRUE
					);
				$this->session->set_userdata($data);

				return TRUE;	
			} else {
				return FALSE;
			}
	}

	public function tambahproduk($foto)
	{
		$data = array(
				'KD_PRODUK' => NULL,
				'NAMA_PRODUK' => $this->input->post('nama'),
				'BERAT' => $this->input->post('berat'),
				'DESKRIPSI' => $this->input->post('deskripsi'),
				'HARGA' => $this->input->post('harga'),
				'foto' => $foto['file_name']
			);

		$this->db->insert('tb_produk', $data);

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		}else{
			return FALSE;
		}	
	}

	public function editproduk($id, $foto = "")
	{
		$data = array(
				'NAMA_PRODUK' => $this->input->post('nama'),
				'BERAT' => $this->input->post('berat'),
				'DESKRIPSI' => $this->input->post('deskripsi'),
				'HARGA' => $this->input->post('harga'),
				'STOCK' => $this->input->post('stock')
			);
		if ($foto != "") {
			$data['FOTO'] = $foto;
		}
		$this->db->where('KD_PRODUK', $id)
						 ->update('tb_produk', $data);

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		}else{
			return FALSE;
		}	
	}

	public function hapusproduk($id)
	{
		$this->db->where('KD_PRODUK', $id)->delete('tb_produk');

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		}else{
			return FALSE;
		}	
	}

	public function getproduk()
	{
		return $this->db->get('tb_produk')
										->result();
	}

	public function getprodukbyid($id)
	{
		return $this->db->where('KD_PRODUK', $id)
										->get('tb_produk')
										->row();
	}

	public function getpembelian()
	{
		return $this->db->get('tb_pembelian')
										->result();
	}

	public function getuser()
	{
		return $this->db->get('tb_user')
										->result();
	}

	public function getbeliview()
	{
		return $this->db->select('tb_pembelian.KD_PEMBELIAN,tb_user.NAMA_USER,tb_pembelian.TOTAL,tb_pembelian.TGL_PEMBELIAN, tb_pembelian.STATUS, tb_pembelian.BUKTI')
										->from('tb_pembelian')
										->join('tb_user','tb_user.KD_USER = tb_pembelian.KD_USER','inner')
										->get()
										->result();
	}

	public function confirmOrder($id)
	{
		$this->db->where('KD_PEMBELIAN', $id)->update('tb_pembelian', [ 'STATUS' => 'SUDAH KONFIRMASI' ]);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function hapusOrder($kd)
	{
		$this->db->where('KD_PEMBELIAN', $kd)->delete('tb_detail');
		$this->db->where('KD_PEMBELIAN', $kd)->delete('tb_pembelian');

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		}else{
			return FALSE;
		}	
	}
}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */