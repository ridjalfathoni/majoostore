<div class="login">
        
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="text-center" style="font-weight: 600; color: #364dd1;">Login</h3>
                        </div>
                        <div class="card-body">
							<form action="<?php echo base_url();?>user/dologin" method="post">
								<?php if (!empty($notif)) { ?>
											<div class="alert alert-danger"><?= $notif; ?></div>
										<?php } ?>
										<?php if (isset($_GET['to']) && $_GET['to'] != "") { ?>
											<input type="hidden" required name="to" value="<?= $_GET['to']; ?>">
										<?php } ?>
								<div class="form-group text-center">
									<input class="form-control" type="text" value="Email" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
								</div>
								<div class="form-group text-center">
									<input class="form-control" type="password" value="Password" name="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
								</div>
								<div class="form-group text-center">
                                    <button class="btn btn-primary" type="submit" name="submit" value="Login">Login</button>
                                </div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>