<div class="container-fluid">
    <div class="page-header">
		<h1>Order List</h1>
	</div>
	<table class="table ">
		<tr>
			<th class="t-head head-it ">Products</th>
			<th class="t-head">Price</th>
			<th class="t-head">Quantity</th>
			<th class="t-head">Total</th>
			<th class="t-head"></th>
		</tr>
		<?php if (!empty($cart)) {
  		foreach ($cart as $c) { ?>
  		<tr class="cross">
			<td class="ring-in">
			<div class="sed">
				<h5><?= $c['NAMA_PRODUK']; ?></h5>
			</div>
			<div class="clearfix"> </div>
			</td>
			<td class="t-data"><?= $c['HARGA']; ?></td>
			<td class="t-data"><div class="quantity"> 
				<div class="quantity-select">            
					<div class="entry value-minus" aria-label="<?= $c['KD_PRODUK']; ?>">&nbsp;</div>
						<div class="entry value"><span class="span-1"><?= $c['JUMLAH']; ?></span></div>									
					<div class="entry value-plus active" aria-label="<?= $c['KD_PRODUK']; ?>">&nbsp;</div>
				</div></div>
			</td>
			<td class="t-data"><?= $c['HARGA']*$c['JUMLAH'] ?></td>
			<td class="t-data">
	      <a type="button" href="<?= base_url('user/removeCart/').$c['KD_PRODUK']; ?>" class="btn btn-danger ">
	        <i class="fa fa-trash"></i> Hapus
	      </a>
	  	</td>
		</tr>
  	<?php }
 		} else { ?>
  		<tr>
	  		<td colspan="3">Cart Kosong</td>
	  	</tr>
  	<?php } ?>
	</table>
	<div class="add add-2">
			<a href="<?php echo base_url();?>user/goOrder">
				<button class="btn btn-danger my-cart-btn my-cart-b" data-image="images/of16.png">Go Checkout</button>
			</a>
	</div>
</div>