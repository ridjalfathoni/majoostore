<div class="container-fluid">
    <div class="page-header">
		<h1>Upload Proof of Payment</h1>
	</div>
	<?php if (!empty($notif)): ?>
    <div class="alert alert-warning"><?= $notif; ?></div>
  <?php endif ?>
  <form action="<?= base_url('user/doConfirm/').$kd_beli; ?>" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <input class="form-control"  type="file" value="" name="foto">
      </div>
    <div class="form-group">
      <button type="submit" class="btn btn-danger my-cart-btn my-cart-b" name="submit">Submit</button>
    </div>
  </form>
</div>