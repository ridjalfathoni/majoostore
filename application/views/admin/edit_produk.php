<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Produk
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- /.row -->
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-success">
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>admin/updateproduk/<?= $produk->KD_PRODUK; ?>">
              <?php
                if (!empty($notif)) {
                  if ($notif=="Update berhasil!") {
                    echo '<div class="alert alert-success">';
                    echo $notif;
                    echo "</div>";
                  }else{
                    echo '<div class="alert alert-danger">';
                    echo $notif;
                    echo "</div>";
                  }
                }
              ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Produk</label>
                  <input type="text" class="form-control" id="biaya" placeholder="Nama Produk" name="nama" value="<?= $produk->NAMA_PRODUK; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Berat (kg)</label>
                  <input type="text" class="form-control" id="berat" placeholder="Berat Produk" name="berat" value="<?= $produk->BERAT; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Deskripsi (kg)</label>
                  <input type="text" class="form-control" id="deskripsi" placeholder="Deskripsi Produk" name="deskripsi" value="<?= $produk->DESKRIPSI; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Harga</label>
                  <input type="text" class="form-control" id="harga" placeholder="Harga Produk" name="harga" value="<?= $produk->HARGA; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Stok</label>
                  <input type="text" class="form-control" id="stok" placeholder="Stok Produk" name="stock" value="<?= $produk->STOCK; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="foto" name="foto">
              </div>
              </div>
              
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>