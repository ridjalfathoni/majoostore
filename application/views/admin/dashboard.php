<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <?php if ($this->session->flashdata('notif')) { ?>
        <div class="alert alert-danger"><?= $this->session->flashdata('notif'); ?></div>
      <?php } ?>
      <h1>
        Dashboard
        <small>Produk</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Produk</span>
              <span class="info-box-number"><?php echo $jml_produk ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa ion-android-close"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">User</span>
              <span class="info-box-number"><?php echo $user ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-android-done"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pembelian</span>
              <span class="info-box-number"><?php echo $beli ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
      <!-- /.row -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Produk</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Kode Produk</th>
                    <th>Nama Produk</th>
                    <th>Berat</th>
                    <th>Deskripsi</th>
                    <th>Stok</th>
                    <th>Harga</th>
                    <th>Foto</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($produk as $data_produk) { ?>
                      <tr>
                        <td id="kd"><?php echo $data_produk->KD_PRODUK ?></td>
                        <td id="nm"><?php echo $data_produk->NAMA_PRODUK ?></td>
                        <td id="b"><?php echo $data_produk->BERAT; ?></td>
                        <td id="d"><?php echo $data_produk->DESKRIPSI; ?></td>
                        <td id="s"><?php echo $data_produk->STOCK; ?></td>
                        <td id="s">Rp<?php echo number_format($data_produk->HARGA,0,',','.'); ?></td>
                        <td id="h" hidden="true"><?php echo $data_produk->HARGA; ?></td>
                        <td><img id="f" style="width: 50px" src="<?php echo base_url() ?>upload/<?php echo $data_produk->FOTO; ?>" alt=""></td>
                        <td>
                          <a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#detailproduk" onclick="lihatproduk(this)"><i class="glyphicon glyphicon-search"></i> Lihat</a>
                          <a href="<?= base_url('admin/deleteproduk/').$data_produk->KD_PRODUK; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin ingin menghapus?')"><i class="glyphicon glyphicon-trash"></i> Hapus</a>
                        </td>
                      </tr> <?php } ?>
                      
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?= base_url('admin/tambahproduk/'); ?>" class="btn btn-sm btn-info btn-flat pull-left">Tambah produk</a>
            </div>
            <!-- /.box-footer -->
          </div>

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Rekap Pembelian</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Kode Pembelian</th>
                    <th>Nama Pembeli</th>
                    <th>Total Pembelian</th>
                    <th>Tanggal Beli</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($pembelian as $databeli) { ?>
                      <tr>
                        <td><?php echo $databeli->KD_PEMBELIAN ?></td>
                        <td><?php echo $databeli->NAMA_USER; ?></td>
                        <td><?php echo $databeli->TOTAL; ?></td>
                        <td><?php echo $databeli->TGL_PEMBELIAN; ?></td>
                        <td><?php echo $databeli->STATUS; ?></td>
                        <td>
                          <?php if ($databeli->STATUS == 'SUDAH TRANSFER'): ?>
                            <button class="btn btn-success" data-toggle="modal" data-target="#confirmImg" data-image="<?= base_url().$databeli->BUKTI; ?>" aria-index="<?= $databeli->KD_PEMBELIAN; ?>" onclick="setModalImg(this)"><i class="fa fa-check"></i> Konfirmasi</button><br>
                          <?php endif ?>
                          <a href="<?= base_url('admin/deleteOrder/'.$databeli->KD_PEMBELIAN)?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin ingin menghapus?')"><i class="glyphicon glyphicon-trash"></i> Hapus</a>
                        </td>
                      </tr> <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="modal fade" id="confirmImg" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">Bukti Pembayaran</div>
                  <div class="modal-body">
                    <img src="" alt="" width="100%">
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-success" href=""><i class="fa fa-check"></i> Konfirmasi</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <div class="modal fade" id="detailproduk">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detail produk</h4>
                  </div>
                  <div class="modal-body">
                    <form  id="dtl_kp" role="form" action="<?= base_url('admin/updateproduk/')?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="nm_kp">Nama produk</label>
                      <input type="text" class="form-control" value="" id="nm_kp" placeholder="Nama produk" name="nama">
                    </div>
                    <div class="form-group">
                      <label for="brt">Berat (kg)</label>
                      <input type="text" class="form-control" value="" id="brt" placeholder="Berat produk" name="berat">
                    </div>
                    <div class="form-group">
                      <label for="brt">Deskripsi</label>
                      <input type="text" class="form-control" value="" id="des" placeholder="Deskripsi produk" name="deskripsi">
                    </div>
                    <div class="form-group">
                      <label for="hrg">Harga</label>
                      <input type="text" class="form-control" value="" id="hrg" placeholder="Harga produk" name="harga">
                    </div>
                    <div class="form-group">
                      <label for="stc">Stock</label>
                      <input type="text" class="form-control" value="" id="stc" placeholder="Stock produk" name="stock">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Foto produk</label>
                          <img style="height:400px;width: auto" id="foto" src="" alt="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputFile">File input</label>
                          <input type="file" id="foto" name="foto">
                          <p class="help-block">Upload Foto produk</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="sbm" value="Save Changes">
                  </div>

                  </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

    </section>
    <!-- /.content -->
  </div>
  <script>
    function setModalImg(e) {
      $('#confirmImg').find('img').attr('src', $(e).attr('data-image'));
      $('#confirmImg').find('.modal-footer a').attr('href', "<?= base_url('admin/confirmOrder/'); ?>" + $(e).attr('aria-index'));
    }
    function lihatproduk(e) {
      var _kd = $(e).parents('tr').find('#kd').text().trim();
      var _nm = $(e).parents('tr').find('#nm').text().trim();
      var _b = $(e).parents('tr').find('#b').text().trim();
      var _d = $(e).parents('tr').find('#d').text().trim();
      var _h = $(e).parents('tr').find('#h').text().trim();
      var _s = $(e).parents('tr').find('#s').text().trim();
      var foto = $(e).parents('tr').find('#f').attr('src');
      var oldAction = $('#dtl_kp').attr('action');
      $('#dtl_kp').attr('action', oldAction + _kd);
      $('#foto').attr('src', foto);
      $('#nm_kp').attr('value', _nm);
      $('#brt').attr('value', _b);
      $('#des').attr('value', _d);
      $('#hrg').attr('value', _h);
      $('#stc').attr('value', _s);
      $('#stc').attr('value', _s);
      console.log(oldAction + _kd);
    }
  </script>