<div class="container-fluid">
    <div class="page-header">
		<h1>Order History</h1>
	</div>
	<table class="table table-hover">
    <thead>
      <tr>
        <th>Kode Pembelian</th>
        <th>Tanggal Pembelian</th>
        <th>Total</th>
        <th>Status</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($list_order as $l) { ?>
      <tr>
        <td><?= $l->KD_PEMBELIAN; ?></td>
        <td><?= $l->TGL_PEMBELIAN; ?></td>
        <td><?= $l->TOTAL; ?></td>
        <td><?= $l->STATUS; ?></td>
        <?php if ($l->STATUS == 'BELUM TRANSFER') { ?>
          <td><button class="btn btn-success" onclick="window.location='<?= base_url(); ?>user/doConfirm/<?= $l->KD_PEMBELIAN; ?>'">CONFIRM</button></td>
          <?php } ?>
      </tr>
      <?php } ?>
      </tbody>
    </table>
</div>