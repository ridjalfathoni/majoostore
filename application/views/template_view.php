<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MaJoo Store</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/style.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url();?>user">Majoo Teknologi Indonesia</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
					<li class="active"><a href="<?php echo base_url();?>user">Product</a></li>
					<?php if ($this->session->userdata('logged_in') == TRUE){?>
						<li><a href="<?= base_url('user/history');?>"> Order History</a></li>
					<?php } ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
					
                    <li>
						<a href="<?php echo base_url();?>user/cart">
							<?php 
								$cartCount = 0;
								if (!empty($this->session->userdata('cart'))) {
									foreach ($this->session->userdata('cart') as $c) {
										$cartCount += $c['JUMLAH'];
									}
								}
							?>
							<span class="glyphicon glyphicon-shopping-cart"></span>
							<span class="badge badge-notify my-cart-badge"><?= $cartCount ?></span>
						</a>
					</li>
					<?php if ($this->session->userdata('logged_in') == TRUE){?>
						<li>
							<a href="<?php echo base_url()?>user/logout"> Log out</a>
						</li>
					<?php } else {?>
						<li>
							<a href="<?php echo base_url()?>user/login"> Login</a>
						</li>
						<li>
							<a href="<?php echo base_url()?>user/register" > Register</a>
						</li>
					<?php } ?>
                </ul>
            </div>
        </div>
    </nav>
	<!--content-->
	<div id="content" class="content-section" style="margin-bottom: 50px">
		<?php
		if (isset($content_view)) {
			$this->load->view($content_view);
		} 
		?>
	</div>
	<!-- Footer -->
	<footer class="page-footer font-small blue pt-4 navbar-fixed-bottom">
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">
            <a style="color: #FFFFFF;"> 2019 © PT MAJOO TEKNOLOGI INDONESIA </a>
        </div>
    </footer>
    <!-- Footer -->
</body>
</html>