<div class="container-fluid">
    <div class="page-header">
		<h1>Product</h1>
	</div>
    <div class="row">
		<?php foreach ($produk as $p) { ?>
			<div class="col-md-3">
				<div class="thumbnail" style="text-align: center">
					<!-- <a href="#" data-toggle="modal" data-target="#<?= $p->KD_PRODUK; ?>" class="offer-img"> -->
					<img src="<?php echo base_url().'upload/'.$p->FOTO;?>"alt="">
					<div class="caption">
						<h3 style="font-weight: 400"><?= $p->NAMA_PRODUK; ?></h3>
					</div>
					<div class="caption" style="margin-top: -20px">
						<h4 style="font-weight: 600;">Rp <?= $p->HARGA; ?></h3>
					</div>
					<div class="caption">
						<p>
							<?= $p->DESKRIPSI; ?>
						</p>
					</div>
					<a href="<?= base_url('user/addToCart/').$p->KD_PRODUK; ?>">
						<button type="button" class="btn btn-success" style="margin: 20px 0px 20px 0px; width: 100px">Beli</button>
					</a>
				</div>
			</div>
		<?php } ?>
    </div>
</div>