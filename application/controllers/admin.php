<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('admin_model');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			if ($this->session->userdata('userlevel') == "admin")
			{
				$produk=$this->admin_model->getproduk();
				$data['user']=count($this->admin_model->getuser());
				$data['beli']=count($this->admin_model->getpembelian());
				$data['produk']=$produk;
				$data['jml_produk']=count($produk);
				$data['pembelian']=$this->admin_model->getbeliview();
				$data['tampilan_admin']="admin/dashboard";
				$this->load->view('admin/t_admin_view', $data);
			}else{
				$this->load->view('restricted');
			}
		}else{
			$this->load->view('login');
		}
	}

	public function tambahproduk()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			if ($this->session->userdata('userlevel') == "admin")
			{
				$data['tampilan_admin'] = 'admin/tambah_produk';
				$this->load->view('admin/t_admin_view', $data);
			}else{
				$this->load->view('restricted');
			}
		}else{
			$this->load->view('login');
		}
	}

	public function insert_produk()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			if ($this->session->userdata('userlevel') == "admin")
			{
				if ($this->input->post('submit')) {
					$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
					$this->form_validation->set_rules('berat', 'Berat', 'trim|required');
					$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
					$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
					$this->form_validation->set_rules('stock', 'Stok', 'trim|required');

					if ($this->form_validation->run() == TRUE) {

						$config['upload_path'] = './upload/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size'] = '2000';

						$this->load->library('upload', $config);

						if ($this->upload->do_upload('foto')) 
						{
							if ($this->admin_model->tambahproduk($this->upload->data()) == TRUE) {
								$data['tampilan_admin'] = 'admin/tambah_produk';
								$data['notif'] = 'Tambah berhasil!';
								$this->load->view('admin/t_admin_view', $data);
							}else{
								$data['tampilan_admin'] = 'admin/tambah_produk';
								$data['notif'] = 'Tambah gagal';
								$this->load->view('admin/t_admin_view', $data);
							}
						}
						else {
						// jika gagal
						$data['notif'] = $this->upload->display_errors();
						$data['tampilan_admin'] = 'admin/tambah_produk';
						$this->load->view('admin/t_admin_view', $data);
					}
				}else{
					$data['notif'] = validation_errors();
					$data['tampilan_admin'] = 'admin/tambah_produk';
					$this->load->view('admin/t_admin_view', $data);
				}
			}else{
				$this->load->view('restricted');
			}
		}else{
			$this->load->view('login');
		}
	}
}

	public function updateproduk($kd)
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			if ($this->session->userdata('userlevel') == "admin")
			{
				if ($this->input->post('sbm')) {
					$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
					$this->form_validation->set_rules('berat', 'Berat', 'trim|required');
					$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
					$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
					$this->form_validation->set_rules('stock', 'Stok', 'trim|required');

						if ($this->form_validation->run() == TRUE) {
								$config['upload_path'] = './upload/';
								$config['allowed_types'] = 'gif|jpg|png|jpeg';
								$config['max_size'] = '2000';

								$this->load->library('upload', $config);

								if ($_FILES['foto']['name'] != "") {
									if ($this->upload->do_upload('foto')) {
										if ($this->admin_model->editproduk($kd, $this->upload->data()['file_name']) == TRUE) {
											$this->session->set_flashdata('notif', 'Update Berhasil');
											redirect('admin');
										}else{
											$this->session->set_flashdata('notif', 'Update Gagal');
											redirect('admin');
										}
									}
									else {
										$this->session->set_flashdata('notif', $this->upload->display_errors());
										redirect('admin');
									}
								} else {
									if ($this->admin_model->editproduk($kd) == TRUE) {
											$this->session->set_flashdata('notif', 'Update Berhasil');
											redirect('admin');
										}else{
											$this->session->set_flashdata('notif', 'Update Gagal');
											redirect('admin');
										}
								}
						}else{
							$this->session->set_flashdata('notif', validation_errors());
							redirect('admin');
						}
				}
			}else{
				$this->load->view('restricted');
			}
		}else{
			$this->load->view('login');
		}
	}

	public function deleteproduk()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			if ($this->session->userdata('userlevel') == "admin")
			{
				if ($this->admin_model->hapusproduk($this->uri->segment(3))) {
					$this->session->set_flashdata('notif', 'Hapus berhasil');
					redirect('admin');
				} else {
					$this->session->set_flashdata('notif', 'Hapus gagal');
					redirect('admin');
				}
			}else{
				$this->load->view('restricted');
			}
		}else{
			$this->load->view('login');
		}
	}

	public function confirmOrder($id)
	{
		if ($this->session->userdata('logged_in') != TRUE || $this->session->userdata('userlevel') != "admin") {
			redirect('admin');
		}
		if ($this->admin_model->confirmOrder($id)) {
			$this->session->set_flashdata('notif', 'Berhasil dikonfirmasi');
			redirect('admin');
		} else {
			$this->session->set_flashdata('notif', 'Gagal dikonfirmasi');
			redirect('admin');
		}
	}

	public function login()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == TRUE)
				{
					if ($this->admin_model->cek_user() == TRUE){
						redirect('admin');
					} else {
						$data['notif'] = 'Login gagal';
						$this->load->view('login', $data);
					}
				// jika sukses

			} else {
				// jika gagal
				$data['notif'] = validation_errors();
				$this->load->view('login', $data);
			}
		}else{
			$this->load->view('login');
		}
	}

	public function deleteOrder($kd)
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			if ($this->session->userdata('userlevel') == "admin")
			{
				if ($this->admin_model->hapusOrder($kd) == TRUE) {
					$this->session->set_flashdata('notif', 'Hapus berhasil');
					redirect('admin');
				} else {
					$this->session->set_flashdata('notif', 'Hapus gagal');
					redirect('admin');
				}
			}else{
				$this->load->view('restricted');
			}
		}else{
			$this->load->view('login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin');
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */